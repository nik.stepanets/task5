﻿Feature: Add product to bag

In order to buy necessary prduct
As a website user
I want to be able to add products to basket

@smoke
Scenario: Clicking on shoes in product list should open product page
	Given I open MEN shoes page
	When I click on any item in list
	Then Corresponding product page with the same description and price should be opened

@smoke
Scenario: After click on "Add To Bag" button on Shoes page without selecting size, an error message should appear
	Given I open MEN shoes page
	When I click "BOOTS" category button
	And I click on any item in list
	And I click ADD TO BAG button
	Then Error message should displayed

@smoke @basket
Scenario: After click on "Add To Bag" button on Shoes page, product should appear in Mini Bag Dropdown
	Given I open MEN shoes page
	When I click "TRAINERS" category button
	And I click on any item in list
	And I select any shoes size
	And I click ADD TO BAG button
	Then ADD TO BAG button changes text to ADDED
	And Mini Bag Dropdown with added item should appear
