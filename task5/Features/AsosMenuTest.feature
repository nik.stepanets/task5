﻿Feature: Test Asos main menu functionality

In order to find necessary product
As a website user
I want to be able to open product categories page through main menu

@mainMenuFunctionality
Scenario Outline: Clicking on product in Men`s category of main menu should open corresponding page
	Given I open Asos website
	When I click MEN button from the main menu
	And I hover the "<menuItem>" menu item from the main menu
	And I click the "<shopByProduct>" link from submenu
	Then Page with "<pageTitle>" title should be opened

	Examples:
	| menuItem | shopByProduct | pageTitle              |
	| Shoes    | View all      | Men's Shoes & Footwear |
	| Sale     | SALE View all | Men's Sale             |
	| Outlet   | View all      | Men's Outlet View All  |


@mainMenuFunctionality
Scenario Outline: Clicking on product in Women`s category of main menu should open corresponding page
	Given I open Asos website
	When I click WOMEN button from the main menu
	And I hover the "<menuItem>" menu item from the main menu
	And I click the "<shopByProduct>" link from submenu
	Then Page with "<pageTitle>" title should be opened

	Examples:
	| menuItem    | shopByProduct | pageTitle                |
	| Dresses     | New in        | Women's New in: Clothing |
	| Face + Body | Gifts         | Face + Body gifts        |