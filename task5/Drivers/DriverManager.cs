﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using RestSharp;
using System.Net;

namespace task5.Drivers
{
    internal static class DriverManager
    {
        public static string HUB_URL = "http://localhost:4444/wd/hub";
        static readonly bool remoteWebDriver = CheckIfServerIsRunning(HUB_URL);
        private static readonly ThreadLocal<IWebDriver> _driver = new ThreadLocal<IWebDriver>();

        static bool CheckIfServerIsRunning(string url)
        {
            RestClient client = new RestClient(url);
            RestRequest request = new RestRequest("/status", Method.Get);
            RestResponse response = client.Execute(request);

            return response != null && response.StatusCode.Equals(HttpStatusCode.OK);
        }

        public static IWebDriver Instance()
        {
            if (_driver.Value == null)
            {
                ChromeOptions options = new ChromeOptions();
                options.AddArgument("--disable-blink-features=AutomationControlled");
                if (remoteWebDriver)
                {
                    Uri uri = new Uri(HUB_URL);
                    _driver.Value = new RemoteWebDriver(uri, options);
                }
                else
                {
                    _driver.Value = new ChromeDriver(options);
                }
            }

            _driver.Value.Manage().Window.Maximize();
            return _driver.Value;
        }

        public static void QuitDriver()
        {
            Instance().Quit();
            _driver.Value = null!;
        }
    }
}
