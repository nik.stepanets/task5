﻿using FluentAssertions.Execution;
using task5.Pages;
using task5.Pages.CommonPages;

namespace task5.StepDefinitions
{
    [Binding]
    public class ProductListPageStepDefinitions
    {
        private readonly ScenarioContext _scenarioContext;
        public ProductListPageStepDefinitions(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        [When(@"I click on any item in list")]
        public void ClickAnyItemInList()
        {
            var itemDetails = MenShoesListPage.Instance.ClickAnyItem();
            _scenarioContext["description"] = itemDetails.description;
            _scenarioContext["price"] = itemDetails.price;
            _scenarioContext["salePrice"] = itemDetails.salePrice;
        }

        [Then(@"Corresponding product page with the same description and price should be opened")]
        public void CorrespondingItemPageShouldBeOpened()
        {
            using (new AssertionScope())
            {
                ShoesItemPage.Instance.GetItemTitle().Should().Be((string)_scenarioContext["description"]);
                ShoesItemPage.Instance.GetItemPrice().Should().Be((string)_scenarioContext["price"]);
                ShoesItemPage.Instance.GetItemSalePrice().Should().Be((string)_scenarioContext["salePrice"]);
            }
        }

        [When(@"I click ""([^""]*)"" category button")]
        public void ClickCategoryButton(string category)
        {
            ProductsListPage.Instance.ClickProductCategory(category);
        }
    }
}
