﻿using task5.Drivers;
using TechTalk.SpecFlow.BindingSkeletons;

namespace task5.StepDefinitions
{
    [Binding]
    public sealed class Hooks
    {
        [AfterScenario]
        public void AfterScenario()
        {
            DriverManager.Instance().Manage().Cookies.DeleteAllCookies();
        }

        [AfterScenario(tags: "basket")]
        public static void AfterScenarioWithBasket()
        {
            DriverManager.QuitDriver();
        }

        [AfterTestRun]
        public static void AfterTestRun()
        {
            DriverManager.QuitDriver();
        }
    }
}