using task5.Pages;

namespace task5.StepDefinitions
{
    [Binding]
    public class AddProductToBagStepDefinitions
    {
        private readonly ScenarioContext _scenarioContext;
        public AddProductToBagStepDefinitions(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        [Given(@"I open MEN shoes page")]
        public void OpenMenShoesPage()
        {
            MenShoesListPage.Instance.OpenPage();
        }

        [When(@"I click ADD TO BAG button")]
        public void ClickAddToBagButton()
        {
            ShoesItemPage.Instance.ClickAddToBagButton();
        }

        [Then(@"Error message should displayed")]
        public void ErrorMessageShouldDisplayed()
        {
            ShoesItemPage.Instance.IsSelectSizeErrorMessageDisplayed().Should().BeTrue(because: "Size is not selected");
        }

        [Then(@"ADD TO BAG button changes text to ADDED")]
        public void AddToBagButtonChangesTextToAdded()
        {
            ShoesItemPage.Instance.IsAddToBagButtonChangesText().Should().BeTrue(because: "[ADD TO BAG] button clicked");
        }

        [When(@"I select any shoes size")]
        public void WhenISelectAnyShoesSize()
        {
            ShoesItemPage.Instance.SelectAnyShoesSize();
        }

        [Then(@"Mini Bag Dropdown with added item should appear")]
        public void MiniBagDropdownWithAddedItemShouldAppear()
        {
            MiniBagDropdownPage.Instance.IsMiniBagDisplayed().Should().BeTrue(because: "New item added to bag");
            MiniBagDropdownPage.Instance.GetItemsDescription().Should().ContainSingle((string)_scenarioContext["description"]);
        }

    }
}
