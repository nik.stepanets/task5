﻿using task5.Pages;

namespace task5.StepDefinitions
{
    [Binding]
    public class HomePageStepDefinitions
    {
        [Given(@"I open Asos website")]
        public void OpenAsosWebsite()
        {
            HomePage.Instance.OpenPage();
        }

        [When(@"I click MEN button from the main menu")]
        public void ClickMenButton()
        {
            HomePage.Instance.ClickShopMenButton();
        }

        [When(@"I click WOMEN button from the main menu")]
        public void WhenIClickWOMENButtonFromTheMainMenu()
        {
            HomePage.Instance.ClickShopWomenButton();
        }

        [When(@"I hover the ""([^']*)"" menu item from the main menu")]
        public void HoverMenuItem(string productCategory)
        {
            HomePage.Instance.HoverMainMenuItem(productCategory);
        }

        [When(@"I click the ""([^']*)"" link from submenu")]
        public void ClickSubmenuLink(string linkText)
        {
            HomePage.Instance.ClickShopByProductLink(linkText);
        }
    }
}
