using task5.Pages.CommonPages;

namespace task5.StepDefinitions
{
    [Binding]
    public class AsosMenuTestStepDefinitions
    {
        [Then(@"Page with ""([^""]*)"" title should be opened")]
        public void PageWithTitleShouldBeOpened(string title)
        {
            ProductsListPage.Instance.IsPageTitleDisplayed(title).Should().BeTrue();
        }
    }
}
