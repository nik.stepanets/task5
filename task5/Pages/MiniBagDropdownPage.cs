﻿using OpenQA.Selenium;
using task5.Drivers;
using task5.Pages.CommonPages;

namespace task5.Pages
{
    public class MiniBagDropdownPage : BasePage
    {
        IWebElement MiniBagDropdown => DriverManager.Instance().FindElement(By.XPath("//div[@id='minibag-dropdown']"));
        IReadOnlyCollection<IWebElement> BagItems => MiniBagDropdown.FindElements(By.XPath(".//li"));

        public static MiniBagDropdownPage Instance { get; } = new MiniBagDropdownPage();

        public bool IsMiniBagDisplayed()
        {
            return IsDisplayed(MiniBagDropdown);
        }

        public string GetItemDescription(IWebElement element)
        {
            return element.FindElement(By.XPath(".//dt[text()='Name']/following-sibling::dd")).Text.Trim();
        }

        public List<string> GetItemsDescription()
        {
            return BagItems.Select(x => GetItemDescription(x)).ToList();
        }
    }
}
