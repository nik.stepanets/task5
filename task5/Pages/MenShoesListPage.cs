﻿using task5.Pages.CommonPages;

namespace task5.Pages
{
    internal class MenShoesListPage : ProductsListPage
    {
        public override string RelativeURL => "/men/shoes-boots-trainers/cat/?cid=4209";

        public static new MenShoesListPage Instance { get; } = new MenShoesListPage();
    }
}
