﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using task5.Drivers;
using task5.Pages.CommonPages;

namespace task5.Pages
{
    public class HomePage : BasePage
    {
        IWebElement ShopMenMenuButton => DriverManager.Instance().FindElement(By.Id("men-floor"));
        IWebElement ShopWomenMenuButton => DriverManager.Instance().FindElement(By.Id("women-floor"));
        IReadOnlyCollection<IWebElement> MenuItems => DriverManager.Instance()
            .FindElements(By.XPath("//nav[not(@aria-hidden)]//button"));
        IWebElement ProductCategories => DriverManager.Instance()
            .FindElement(By.XPath("//button[@aria-expanded='true']/following-sibling::div[1]"));

        public static HomePage Instance { get; } = new HomePage();

        public void ClickShopMenButton()
        {
            ShopMenMenuButton.Click();
        }

        public void ClickShopWomenButton()
        {
            ShopWomenMenuButton.Click();
        }
        public void HoverMainMenuItem(string item)
        {
            var actions = new Actions(DriverManager.Instance());
            var menuItem = MenuItems.Single(x => x.FindElement(By.XPath("./span/span")).Text.Equals(item));

            actions.MoveToElement(menuItem).Perform();
        }

        public void ClickShopByProductLink(string linkText)
        {
            ProductCategories.FindElement(By.LinkText(linkText)).Click();
        }
    }
}
