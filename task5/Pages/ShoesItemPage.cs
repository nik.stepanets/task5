﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using task5.Drivers;
using task5.Pages.CommonPages;

namespace task5.Pages
{
    public class ShoesItemPage : ItemPage
    {
        IWebElement ShoesSizeDropdown => DriverManager.Instance().FindElement(By.XPath("//select[@data-id='sizeSelect']"));
        IWebElement SelectSizeError => DriverManager.Instance().FindElement(By.Id("selectSizeError"));
        
        public static ShoesItemPage Instance { get; } = new ShoesItemPage();

        public string GetSelectedSize()
        {
            return ShoesSizeDropdown.FindElements(By.TagName("option")).Single(x => x.Selected).Text;
        }

        public bool IsSelectSizeErrorMessageDisplayed()
        {
            return IsDisplayed(SelectSizeError);
        }

        public void SelectShoesSize(string text)
        {
            SelectElement shoesSizeSelect = new SelectElement(ShoesSizeDropdown);
            shoesSizeSelect.SelectByText(text);
        }
        
        public void SelectAnyShoesSize()
        {
            var sizes = ShoesSizeDropdown.FindElements(By.XPath("./option[not(@disabled)]")).Skip(1);
            var randomSize = sizes.ElementAt(rand.Next(sizes.Count())).Text;
            SelectShoesSize(randomSize);
        }
    }
}
