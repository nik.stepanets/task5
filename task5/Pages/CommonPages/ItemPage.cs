﻿using OpenQA.Selenium;
using task5.Drivers;

namespace task5.Pages.CommonPages
{
    public class ItemPage : BasePage
    {
        IWebElement AddToBagButton => DriverManager.Instance().FindElement(By.Id("product-add-button"));
        IWebElement ItemTitle => DriverManager.Instance().FindElement(By.XPath("//main//h1"));
        string CurrentPrice => DriverManager.Instance()
            .FindElement(By.XPath("//div[@data-test-id='product-price']//span[@data-test-id='current-price']")).Text;
        string PreviousPrice
        {
            get
            {
                var elements = DriverManager.Instance()
            .FindElements(By.XPath("//div[@data-test-id='product-price']//span[@data-test-id='previous-price']"));
                return elements.Count > 0 ? elements.First().Text : "";
            }
        }

        public string GetItemTitle()
        {
            return ItemTitle.Text;
        }

        public void ClickAddToBagButton()
        {
            AddToBagButton.Click();
        }

        public bool IsAddToBagButtonChangesText()
        {
            try
            {
                Wait.Until(driver => AddToBagButton.Text.Contains("ADDED"));
                return true;
            }
            catch (WebDriverTimeoutException)
            {
                return false;
            }
        }

        public string GetItemPrice()
        {
            return PreviousPrice != "" ? PreviousPrice : CurrentPrice;
        }

        public string GetItemSalePrice()
        {
            return PreviousPrice == "" ? "" : CurrentPrice;
        }
    }
}
