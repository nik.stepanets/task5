﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using task5.Drivers;

namespace task5.Pages.CommonPages
{
    public abstract class BasePage
    {
        public string BaseURL => "https://www.asos.com";
        public virtual string RelativeURL => "/";
        protected WebDriverWait Wait => new WebDriverWait(DriverManager.Instance(), TimeSpan.FromSeconds(5));
        protected Random rand = new Random();
        public void OpenPage()
        {
            DriverManager.Instance().Navigate().GoToUrl($"{BaseURL}{RelativeURL}");
        }
        public bool IsDisplayed(IWebElement element)
        {
            try
            {
                return Wait.Until(driver => element.Displayed);
            }
            catch (WebDriverTimeoutException)
            {
                return false;
            }
        }
    }
}
