﻿using OpenQA.Selenium;
using task5.Drivers;

namespace task5.Pages.CommonPages
{
    public class ProductsListPage : BasePage
    {
        IReadOnlyCollection<IWebElement> Items => DriverManager.Instance().FindElements(By.XPath("//article"));
        IReadOnlyCollection<IWebElement> CategoryButtons => DriverManager.Instance()
            .FindElements(By.XPath("//section[@id='category-banner-wrapper']//a"));
        IWebElement PageTitle => DriverManager.Instance().FindElement(By.XPath("//main//h1"));

        public static ProductsListPage Instance { get; } = new ProductsListPage();

        public bool IsPageTitleDisplayed(string pageTitle)
        {
            return IsDisplayed(PageTitle) && PageTitle.Text.Contains(pageTitle);
        }
        public string GetItemDescription(IWebElement element)
        {
            return element.FindElement(By.XPath(".//h2")).Text;
        }

        public string GetItemPrice(IWebElement element)
        {
            return element.FindElement(By.XPath(".//span[@data-auto-id='productTilePrice']")).Text;
        }

        public string GetItemSalePrice(IWebElement element)
        {
            var elements = element.FindElements(By.XPath(".//span[@data-auto-id='productTileSaleAmount']"));
            return elements.Count > 0 ? elements.First().Text : "";
        }

        public (string description, string price, string salePrice) ClickAnyItem()
        {
            if (Items.Count == 0)
            {
                Wait.Until(driver => Items.Count > 0);
            }
            var randomIndex = rand.Next(Items.Count);
            var randomItem = Items.ElementAt(randomIndex);
            var itemDetails = (description: GetItemDescription(randomItem), price: GetItemPrice(randomItem), salePrice: GetItemSalePrice(randomItem));
            randomItem.Click();

            return itemDetails;
        }

        public void ClickProductCategory(string category)
        {
            CategoryButtons.Single(x => x.Text.Equals(category)).Click();
        }
    }
}
